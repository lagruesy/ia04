---
tags: TD
---

# [IA04] TD 8 - Théorie des jeux : Meilleure réponse et équilibre de Nash

## I. Prélude : la guerre des sexes, le retour

On considère le jeu à deux joueurs suivant (version asymétrique de la *guerre des sexes*)

| Alice\\Bob     | Ballet | Foot |
|------|-------------|------------|
| Ballet | 1 \\ 3 | 0 \\ 0 |
| Foot | 0 \\ 0 | 2 \\ 1 |

1. Quels sont les équilibre de Nash en stratégies pures ?
2. Quels sont les équilibres de Nash en stratégies mixtes ?
3. Pour chacun de ces équilibres, quel est le gain espéré de chacun des agents ?
4. Proposer un équilibre corrélé et calculer le gain espéré des agents


## II. Pierre, Feuille, Ciseaux

1. Représenter le jeu de *pierre, feuille, ciseaux* (PFC) sous forme normale.
2. Proposer une représentation pour une stratégie pure, puis pour une stratégie mixte.
3. Calculer la meilleure réponse en stratégie pure, puis en stratégie mixte à une stratégie mixte de l'adversaire donnée.
4. Donner les équilibres de Nash en stratégies pures et en stratégies mixtes.
